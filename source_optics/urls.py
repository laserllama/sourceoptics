# Copyright 2018-2019 SourceOptics Project Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from django.urls import include, path
from source_optics.views import views


urlpatterns = [

    # top level pages - object hierarchy navigation
    path('', views.list_orgs, name='list_orgs'),

    # FIXME: use org in query string, else show all repos
    path('org/<org>/repos', views.list_repos, name='list_repos'),

    path('author/<author>', views.author_index, name='author_index'),

    path('org/<org>/authors', views.list_authors, name='list_authors'),

    path('repo/<repo>', views.repo_index, name='repo_index'),

    path('pies', views.pie_charts, name='pie_charts'),

    # TODO: see if this is used anywhere, and if not, remove it.
    path('graph/path_segment', views.graph_path_segment, name='graph_path_segment'),

    # REPORTS AND GRAPH PAGES - RATHER FLEXIBLE BY QUERY STRING
    path('report/stats', views.report_stats, name='report_author_stats'),
    path('report/commits', views.report_commits, name='report_commits'),

    # FIXME: directory view and single_file can probably be consolidated to one URL
    path('report/files', views.report_files, name='report_files'),
    path('report/file', views.report_single_file, name='report_single_file'),

    # TODO: rename this as 'graph'
    path('graph2', views.graph_page, name='graph_page'),
    path('graph2/custom', views.graph_custom, name='graph_custom'),

    # Webhooks - haven't been used in a while, could possibly remove.
    path('webhook', views.webhook_post, name='webhook_post'),
    path('webhook/', views.webhook_post, name='webhook_post'),

]
