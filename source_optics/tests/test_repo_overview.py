from django.test import TestCase
from django.http import HttpRequest


from source_optics.tests.view_test_case import ViewTestCase

class RepoViewTests(ViewTestCase):

    
    def test_default_url(self):
        params = {"start": "1970-01-01", "end": "2020-10-10"}
        response = self.client.get("/repo/testrepo", params)
        self.assertEquals(response.status_code, 200)