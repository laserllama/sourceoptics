from django.test import TestCase
from django.http import HttpRequest
from django.urls import reverse


from source_optics.models import File
from source_optics.models import FileChange

from source_optics.tests.view_test_case import ViewTestCase


class FileViewTests(ViewTestCase):

    def setUp(self):
        ViewTestCase.setUp(self)
        self.test_file = File.objects.create(repo = self.test_repo, name = "test_file")
        self.test_file_change = FileChange.objects.create(file = self.test_file, commit = self.test_commit)
        self.test_directory = File.objects.create(repo = self.test_repo, name = "test_directory")
        self.dir_1 = File.objects.create(repo = self.test_repo, name = "dir_1")
        
        self.file_1_1 = File.objects.create(repo = self.test_repo, path = self.dir_1.name, name = "file_1_1")
        self.dir_1_2 = File.objects.create(repo = self.test_repo, path = self.dir_1.name, name = "dir_1_2")
        
        self.file_1_2_1 = File.objects.create(repo = self.test_repo, path = self.dir_1_2.name, name = "file_1_2_1")

        pass

    def tearDown(self):
        # Clean up run after every test method.
        pass

    def test_default_url(self):
        params = {"repo": self.test_repo.id, "start": self.start_date, "end": self.end_date, "path": ""}
        response = self.client.get("/report/files", params)
        self.assertEquals(response.status_code, 200)

    def test_single_file(self):
        params = {"repo": self.test_repo.id, "start": self.start_date, "end": self.end_date, 
        "path": "", "file": self.test_file.name}
        response = self.client.get("/report/file", params)
        self.assertEquals(response.status_code, 200)

    def test_file_in_directory(self):
        params = {"repo": self.test_repo.id, "start": self.start_date, "end": self.end_date, 
        "path": self.dir_1_2.name, "file": self.file_1_2_1.name}
        response = self.client.get("/report/file", params)
        self.assertEquals(response.status_code, 200)