from django.test import TestCase
from django.http import HttpRequest
from django.urls import reverse

from source_optics.views import graphs
from source_optics.tests.view_test_case import ViewTestCase

class GraphViewTests(ViewTestCase):

    
    def test_default_url(self):
        params = {"repo": self.test_repo.id, "start": "1970-01-01", "end": "2020-10-10"}
        response = self.client.get("/graph2", params)
        self.assertEquals(response.status_code, 200)
    
    def test_full_params(self):
        params = {"repo": self.test_repo.id, "start": "1970-01-01", "end": "2020-10-10", 
                  "xaxis": "Time", "yaxis": "Commit Total", "graph_type": "Stacked"}
        response = self.client.get("/graph2", params)
        self.assertEquals(response.status_code, 200)

    # This URL loads the same graph as /graph but fullscreen. 
    def test_graph2_custom(self):
        params = {"repo": self.test_repo.id, "start": "1970-01-01", "end": "2020-10-10", 
                  "xaxis": "Time", "yaxis": "Commit Total", "graph_type": "Stacked"}
        response = self.client.get("/graph2/custom", params)
        self.assertEquals(response.status_code, 200)
    
    def test_3d_graph(self): 
        params = {"repo": self.test_repo.id, "start": "1970-01-01", "end": "2020-10-10", 
                  "xaxis": "Lines Added", "yaxis": "Lines Changed", 
                  "zaxis": "Lines Removed","graph_type": "Scatter"}
        response = self.client.get("/graph2", params)
        self.assertEquals(response.status_code, 200)
        
    def test_compare_repos(self):
        repo_str = self.test_repo.name + self.test_repo2.name
        params = {"repos": repo_str, "start": "1970-01-01", "end": "2020-10-10", 
                  "xaxis": "Lines Added", "yaxis": "Lines Changed", 
                  "zaxis": "Lines Removed","graph_type": "Scatter"}
        response = self.client.get("/graph2", params)
        self.assertEquals(response.status_code, 200)

