from django.test import TestCase
from django.http import HttpRequest
from datetime import date


from source_optics.models import Credential
from source_optics.models import Organization
from source_optics.models import Repository
from source_optics.models import Author
from source_optics.models import Commit
from source_optics.models import Log

class ViewTestCase(TestCase):

    def setUp(self):
        self.start_date = "1970-01-01"
        self.end_date = str(date.today())
        self.test_credential = Credential.objects.create(name = "test credential", username = "test username")
        self.test_org = Organization.objects.create(name = "test org")
        self.test_repo = Repository.objects.create(name = "testrepo", organization = self.test_org)
        self.test_repo2 = Repository.objects.create(name = "testrepo2", organization = self.test_org)
        self.test_author = Author.objects.create(email = "author@email.net", display_name = "author display name")
        self.test_commit = Commit.objects.create(repo = self.test_repo, author = self.test_author)
        self.test_log = Log.objects.create(log_id = 1, organization = self.test_org, author = self.test_author)
        pass

    def tearDown(self):
        # Clean up run after every test method.
        pass