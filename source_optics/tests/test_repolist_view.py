from django.test import TestCase
from django.http import HttpRequest
from django.urls import reverse

from source_optics.tests.view_test_case import ViewTestCase


class TestRepolistView(ViewTestCase):


    def test_default_url(self):
        params = {"start": "1970-01-01", "end": "2020-10-10"}
        url = "/org/" + self.test_org.name + "/repos"
        response = self.client.get(url, params)
        self.assertEquals(response.status_code, 200)