from django.test import TestCase
from django.http import HttpRequest
from django.urls import reverse

from source_optics.tests.view_test_case import ViewTestCase


class TestAuthorView(ViewTestCase):


    def test_default_url(self):
        params = {}
        response = self.client.get("/author/" + self.test_author.email, params)
        self.assertEquals(response.status_code, 200)

    def test_author_graphs(self):
        params = {"author": self.test_author.id, "repo": self.test_repo.id,
        "start": self.start_date, "end": self.end_date, "xaxis": "Time", "yaxis": "Commit Total",
        "graph_type": "Scatter"}
        response = self.client.get("/graph2", params)
        self.assertEquals(response.status_code, 200)

    def test_author_stats(self):
        params = {"author": self.test_author.id, "start": self.start_date, "end": self.end_date}
        response = self.client.get("/report/stats", params)
        self.assertEquals(response.status_code, 200)
    
    def test_author_commits(self):
        params = {"author": self.test_author.id, "start": self.start_date, "end": self.end_date}
        response = self.client.get("/report/commits", params)
        self.assertEquals(response.status_code, 200)
    
    def test_author_logs(self):
        params = {"author": self.test_author.id, "start": self.start_date, "end": self.end_date}
        response = self.client.get("/report/log_details", params)
        self.assertEquals(response.status_code, 200)