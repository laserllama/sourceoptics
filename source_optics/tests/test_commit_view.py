from django.test import TestCase
from django.http import HttpRequest
from django.urls import reverse


from source_optics.tests.view_test_case import ViewTestCase


class CommitViewTests(ViewTestCase):

    def test_default_url(self):
        params = {"repo": self.test_repo.id, "start": "1970-01-01", "end": "2020-10-10"}
        response = self.client.get("/report/commits", params)
        self.assertEquals(response.status_code, 200)