# Copyright 2018-2020 SourceOptics Project Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# sparklines.py - generate sparklines graphs as HTML snippets

from datetime import timedelta, timezone
from .scope import Scope
from ..models.statistic import Statistic
from source_optics.models import Repository
from . import reports
from source_optics.models import Commit

def get_sparklines(scope, repo, start=None, end=None):
    #initial math to calculate how many days each point will represent 
    time_range = (end - start)
    num_points = time_range.days
    if num_points > 20:
        num_points = 20
    boundary_length = time_range/num_points
    delta = timedelta(days=boundary_length.days)
    graph_points = []
    #return all the repo commits
    commit_dates = Commit.objects.filter(repo__name=repo, commit_date__range=(start,end)).values_list('commit_date', flat=True).order_by('commit_date')

    #math: interate thought the commits for x number of points, adding & then averaging the number of commis per each period
    for i in range(num_points):
        boundary_min = (start + (delta * i)).replace(tzinfo=timezone.utc)
        boundary_max = (start + (delta * (i + 1))).replace(tzinfo=timezone.utc)
        total_commits_in_boundary = 0
        for entry in commit_dates:
            if entry >= boundary_min and entry < boundary_max:
                total_commits_in_boundary += 1
        point = total_commits_in_boundary / boundary_length.days
        graph_points.append(str(point))
    #need to return string for sparklines to work
    points_string = ","
    points_string = points_string.join(graph_points)
    return points_string