
import json, copy
from source_optics.models import Repository, Statistic, Commit, Author, File, FileChange
from django.core.paginator import Paginator, EmptyPage
from . import sparklines

def files(scope):
    # the files report view
    repo = scope.repo
    path = scope.path
    if path == '/':
        path = ''

    kids = get_child_paths(scope, repo, path)
    files = get_files(scope, repo, path)

    if path == "":
        path = "/"

    results = []

    # Loops through the subdirectories and gets their names  
    # to be added to the results dict
    for paths in kids:
        (commit_length, author_length) = get_total_commits_and_total_authors(scope, "", paths['path'])
        directory_commit = get_latest_directory_commit(scope, paths['path'])
        date = str(directory_commit.commit_date)
        commit_msg = directory_commit.subject
        commit_author = directory_commit.author.display_name
        if paths['path'] == '':
            paths['path'] = 'root'
        results.append(dict(
            file_name=(paths['path'], "subdirectory"),
            file_date=date,
            file_commit_msg=commit_msg,
            file_commit_author=commit_author,
            file_commit_length=commit_length,
            file_author_length=author_length,
            icon='folder'
        ))

    # Loops through the files and extracts the commit object
    # in order to get the date, message, and author as well as gets
    # the name of the file and adds it to the results dict
    for single_file in files:
        # Checks to make sure the file has not been deleted 
        if not single_file['is_deleted']: 
            date = str(single_file['commit'].commit_date)
            commit_msg = single_file['commit'].subject
            commit_author = single_file['commit'].author.display_name
            (commit_length, author_length) = get_total_commits_and_total_authors(scope, single_file['filename'], single_file['path'])
            results.append(dict(
                file_name=(single_file['filename'], "single_file"),
                file_date=date,
                file_commit_msg=commit_msg,
                file_commit_author=commit_author,
                file_commit_length=commit_length,
                file_author_length=author_length,
                icon='file'
            ))
    
    return dict(
        results=results,
        repo=repo,
        path=path,
        browse_paths=kids,
        paths_length=len(kids),
        browse_files=files,
        files_length=len(files)
    )

def file_scope(scope, filename, path):
    """
    Helper method to get a temporary scope
    for each file and directory in the file view
    """
    temp_scope = copy.copy(scope)
    temp_scope.file = filename
    temp_scope.path = path
    return temp_scope

def get_latest_directory_commit(scope, path):
    """
    Gets the latest commit for each directory
    """
    temp_scope = file_scope(scope, "", path)
    commit_feed = commit_filter(temp_scope.repo, temp_scope.author, temp_scope.org, temp_scope.start, temp_scope.end, temp_scope)
    return commit_feed[0]


def get_total_commits_and_total_authors(scope, filename, path):
    """
    Gets the total number of commits
    and authors for a specific file or directory
    """
    author_list = []
    temp_scope = file_scope(scope, filename, path)
    commit_feed = commit_filter(temp_scope.repo, temp_scope.author, temp_scope.org, temp_scope.start, temp_scope.end, temp_scope)

    for commit in commit_feed.all():
        if commit.author.pk not in author_list:
            author_list.append(commit.author.pk)
    
    return (len(commit_feed), len(author_list))

def get_child_paths(scope, repo, path):

    # find all the directory paths
    all_paths = FileChange.objects.filter(
        commit__repo=repo,
        file__path__startswith=path,
        commit__commit_date__range=(scope.start, scope.end)
    ).values_list('file__path', flat=True).distinct().all()

    slashes = path.count('/')
    desired = slashes
    if path != "":
        desired += 1

    # find all the paths that are one level deeper than the specified path
    # the removal of "=>" deals with moved path detection in old versions of the program w/ legacy data
    children = [ dict(path=path) for path in sorted(all_paths) if ((not '=>' in path) and (path.count('/') == desired)) ]

    return children

def get_files(scope, repo, path):

    all_files = FileChange.objects.filter(
        commit__repo=repo,
        file__path=path,
        commit__commit_date__range=(scope.start, scope.end)
    ).values_list('file', flat=True).distinct().order_by('file__name').all()

    all_files = File.objects.filter(pk__in=all_files)

    return [ dict(filename=f.name, path=f.path, commit=f.created_by, is_deleted=f.deleted) for f in all_files.all() ]

def get_page(objs, scope):
    """
    Helper method for commits_feed and commit_log_summary that gets the
    current page based on the objects and the size of the page.
    """
    paginator = Paginator(objs, scope.page_size)
    try:
        page = paginator.page(scope.page)
    except EmptyPage as e:
        page = paginator.get_page(scope.page)
    return page

def commit_filter(repo, author, organization, start, end, scope):
    """
    Helper method for commits_feed and commit_log_summary that filters commit objects
    by repo & author, repo, author, or organization (in that order) and then filtered further by date
    """
    # FIXME: this looks like a method we should add to Scope() but only to be called when needed
    if repo and author:
        objs = Commit.objects.filter(repo=repo, author=author)
    elif repo:
        objs = Commit.objects.filter(repo=repo)
    elif author:
        objs = Commit.objects.filter(author=author)
    elif organization:
        objs = Commit.objects.filter(repo__organization=organization)
    else:
        raise NotImplementedError("Unexpected filter options for commit_filter")
    if start and end:
        objs = objs.filter(commit_date__range=(start,end))
    
    if scope.path:
        objs = objs.filter(file_changes__file__path=scope.path)
    if scope.file:
        objs = objs.filter(file_changes__file__name=scope.file)
    if scope.extension:
        objs = objs.filter(file_changes__file__ext=scope.extension)

    # all this nested filtering apparently can make bad queries, so we should probably unroll all of the above?

    objs = objs.select_related('author').order_by('-commit_date')
    return objs

def commits_feed(scope):
    objs = None

    repo = scope.repo
    author = scope.author
    organization = scope.org
    start = scope.start
    end = scope.end

    objs = commit_filter(repo, author, organization, start, end, scope)
    count = objs.count()
    page = get_page(objs, scope)

    results = []
    # we may wish to show filechange info here
    for commit in page:
        desc = commit.subject
        if desc is None:
            desc = ""
        desc = desc[:255]
        results.append(dict(
            repo=commit.repo.name,
            commit_date=str(commit.commit_date),
            author_id=commit.author.pk,
            author=commit.author.email,
            author_name=commit.author.display_name,
            sha=commit.sha,
            subject=desc
        ))

    return dict(results=results, page=page, count=count)

def commit_log_summary(scope):
    """
    This method provides commit and log summary objects 
    (custom object with author, commit summary, log summary)
    to views for display in log page table
    Returns dict of results of these author-commit-log summary objects, page data, and total count
    """
    objs = None

    repo = scope.repo
    author = scope.author
    organization = scope.org
    start = scope.start
    end = scope.end

    # first get commit objs
    commit_objs = commit_filter(repo, author, organization, start, end, scope)
    # Use the collection of commits to get these authors
    author_ids = set(commit_objs.values_list('author', flat=True))
    author_objects = Author.objects.filter(pk__in=author_ids)
    author_objects = author_objects.order_by('display_name')

    page = get_page(author_objects, scope)

    count = 0
    results = []
    # at this point page is defined too; all fields to return are defined,
    # but results still needs to be defined

    for indiv_author in page:
        count = count + 1 # for row count
        # make a copy of the scope so we can use it in methods without modifying it here
        temp_scope = copy.copy(scope)
        # create a dictionary representation of the author's commit-log summary and append to results
        indiv_author_summary = process_summary_for_author(indiv_author, temp_scope, commit_objs)
        results.append(indiv_author_summary)

    assert scope.author == None # making sure our temp scope didn't make unwanted changes
    return dict(results=results, page=page, count=count)

def process_summary_for_author(indiv_author, scope, commit_objs):
    '''
    A helper method for processing commit-log summaries for each author
    indiv_author - an Author object to process a commit-log summary for
    scope - a scope object
    commit_objs - a query set of commits to filter through for the author
    '''
    # get the author's commits
    author_commits = commit_objs.filter(author=indiv_author) # just go through the commits we already singled out
    author_commit_count = author_commits.count() # and count the author's commits too

    # get the file count for commits for author
    scope.author = indiv_author # TEMPORARY - to be reset before method returns
    stats_data = author_stats_table(scope)
    file_count = get_num_files(stats_data)

    
    # wrapping the author's data all together
    return dict(
        author_id=indiv_author.id,
        author=indiv_author.display_name,
        commit_summary="%s commits in %s files" % (author_commit_count, file_count),
    )

def get_num_files(stats_data):
    """
    Calculates the number of file change events an author has created, edited, or moved for the commits
    header.
    """
    # FIXME: can we stop using this?
    if len(stats_data) == 1:
        num_files = stats_data[0]['file_changes']
    else:
        num_files = 0
    return num_files

def _annotations_to_table(stats, primary, lookup):

    """
    Processes a list of statistics objects and converts annotated values
    from those statistics objects back to the field names used in the tables.
    """

    results = []

    for entry in stats:
        new_item = {}
        new_item[primary] = entry[lookup]
        for (k,v) in entry.items():
            if k.startswith("annotated_"):
                k2 = k.replace("annotated_","")
                if 'date' in k2 or 'last_scanned' in k2 or 'name' in k2:
                    new_item[k2] = str(v)
                else:
                    new_item[k2] = v
        # for making things easy with ag-grid, the primary key is available multiple times:
        for x in [ 'details1', 'details2', 'details3']:
            new_item[x] = entry[lookup]

        # FIXME: I don't like this is happening here in a non-generic way, but ah well for now, we should
        # move this to a method in the stats class like "non_annotated_stats" or something?
        latest_date = entry['annotated_latest_commit_date']
        earliest_date = entry['annotated_earliest_commit_date']
        if(latest_date and earliest_date):
            new_item["longevity"] = (latest_date - earliest_date).days
        else:
            new_item["longevity"] = 0

        results.append(new_item)
    return results


def author_stats_table(scope, limit=None):
    """
    this drives the author tables, both ranged and non-ranged, accessed off the main repo list.
    the interval 'LF' shows lifetime stats, but elsewhere we just do daily roundups, so this parameter
    should really be a boolean.  The limit parameter is not yet used.
    """

    # FIXME: this performs one query PER author and could be rewritten to be a LOT more intelligent.

    (repos, authors) = scope.standardize_repos_and_authors()
    interval = 'DY'
    stats = Statistic.queryset_for_range(repos=repos, authors=authors, start=scope.start, end=scope.end, interval=interval)
    data = None
    if not scope.author:
        stats = Statistic.annotate(stats.values('author__email')).order_by('author__email')
        data = _annotations_to_table(stats, 'author', 'author__email')
    else:
        stats = Statistic.annotate(stats.values('repo__name')).order_by('repo__name')
        data = _annotations_to_table(stats, 'repo', 'repo__name')
    return data


# FIXME: see what's up with the author table

OBSOLETE = """
def author_stats_punchcard(scope):
    
    # Aggregates statistics about authors and the number of commits they've made
    # each day so that a punchcard can be displayed.


    # FIXME: this performs one query PER author and could be rewritten to be a LOT more intelligent.

    (repos, authors) = scope.standardize_repos_and_authors()
    interval = 'DY'
    stats = Statistic.queryset_for_range(repos=repos, authors=authors, start=scope.start, end=scope.end, interval=interval).values()
    stats = Statistic.annotate_for_punchcard(stats.values('author__email')).order_by('author__email')

    paginator = Paginator(stats, scope.page_size)
    page = paginator.page(scope.page)

    results = []

    for entry in page:
        # each entry is an author
        new_entry = dict()
        new_entry['author_email'] = entry['author__email']
        new_entry['author_name'] = entry['annotated_author_name']
        # these stats are TOTALS for the author
        new_entry['lines_added'] = entry['annotated_lines_added']
        new_entry['lines_removed'] = entry['annotated_lines_removed']
        new_entry['lines_changed'] = entry['annotated_lines_changed']
        new_entry['commits'] = []
        new_entry['logs'] = []
        # now for commit data
        commits = [commit for commit in Commit.objects.filter(
            author=entry['annotated_author_id'], repo=scope.repo, commit_date__range=(scope.start, scope.end)).annotate(
                lines_added_sum=Sum('file_changes__lines_added'),lines_removed_sum=Sum('file_changes__lines_removed')).values()]
        for commit in commits:
            relevant_commit_data = helper_annotate_commit(commit)
            new_entry['commits'].append(relevant_commit_data)
        # end commit data
        # begin log data
        logs = [log for log in Log.objects.filter(author=entry['annotated_author_id'], organization=scope.org, date__range=(scope.start, scope.end)).values()]
        for log in logs:
            # making this a dict to make it consistent with treatment of commits and to be more flexible in the future
            relevant_log_data = dict()
            relevant_log_data['log_date'] = str(log['date'])
            relevant_log_data['log_hours'] = log['hours']
            new_entry['logs'].append(relevant_log_data)
        # end log data
        results.append(new_entry)

    return [entry for entry in results]
"""

def helper_annotate_commit(commit):
    '''
    Params: commit is a dictionary representation of a commit object
    Returns: A dictionary from the data in that commit, including its date
        as well as the # of lines added, removed, and changed for that commit.
    '''
    # these are put into individual dicts with the commit_date so that the lines added/removed/changed is kept with its associated date
    relevant_commit_data = dict()
    relevant_commit_data['commit_date'] = str(commit['commit_date'])
    if(commit['lines_added_sum']):
        relevant_commit_data['lines_added'] = commit['lines_added_sum']
    else:
        relevant_commit_data['lines_added'] = 0
    if(commit['lines_removed_sum']):
        relevant_commit_data['lines_removed'] = commit['lines_removed_sum']
    else:
        relevant_commit_data['lines_removed'] = 0
    relevant_commit_data['lines_changed'] = relevant_commit_data['lines_added'] + relevant_commit_data['lines_removed']
    return relevant_commit_data

def repo_table(scope): # repos, start, end):

    #this drives the list of all repos within an organization, showing the statistics for them within the selected
    #time range, along with navigation links.

    (repos, authors) = scope.standardize_repos_and_authors()
    interval = 'DY'
    # FIXME: explain
    repos = [ x.pk for x in scope.available_repos.all() ]
    stats = Statistic.queryset_for_range(repos=repos, authors=authors, start=scope.start, end=scope.end, interval=interval)
    stats = Statistic.annotate(stats.values('repo__name')).order_by('repo__name')
    data = _annotations_to_table(stats, 'repo', 'repo__name')

    # FIXME: insert in author count, which is ... complicated ... this can be optimized later
    # we should be able to grab every repo and annotate it with the author count in one extra query tops
    # but it might require manually writing it.
    for d in data:
        repo = d['repo']
        author_count = Author.author_count(repo, start=scope.start, end=scope.end)
        d['author_count'] = author_count
        sparkline_numbers = sparklines.get_sparklines(scope, repo, start=scope.start, end=scope.end)
        d['sparklines'] = sparkline_numbers

    # some repos won't have been scanned, and this requires a second query to fill them into the table
    repos = Repository.objects.filter(last_scanned=None, organization=scope.org)
    for unscanned in repos:
        data.append(dict(
            repo=unscanned.name
        ))
    return data


def orgs_table(scope):

    results = []
    for org in scope.orgs:
        row = dict()

        row['name'] = org.name
        row['repo_count'] = (org.repos.count(), org.name)
        row['details1'] = org.pk
        authors = set()
        for repo in org.repos.all():
            authors.update(Author.authors(repo, start=scope.start, end=scope.end))
        row['author_count'] = ( len(authors), org.name )
        results.append(row)
    return json.dumps(results)
    
def author_table(scope):
    (repos, authors) = scope.standardize_repos_and_authors()
    interval = 'DY'
    stats = Statistic.queryset_for_range(repos=None, authors=authors, start=scope.start, end=scope.end, interval=interval)
    data = None
    stats = Statistic.annotate(stats.values('author__email')).order_by('author__email')
    data = _annotations_to_table(stats, 'author', 'author__email')    
    return data