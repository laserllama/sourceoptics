
function queryStringToJSON() {
    var pairs = location.search.slice(1).split('&');

    var result = {};
    pairs.forEach(function(pair) {
        pair = pair.split('=');
        result[pair[0]] = decodeURIComponent(pair[1] || '');
    });

    return JSON.parse(JSON.stringify(result));
}

var query_string_json = queryStringToJSON();
var query_params = [];
var query_string = "?"

//selecting all in the dropdown disables the number input
$(function () {
    $("#datepicker_range").change(function () {
        if ($(this).val() == "all") {
            $("#datepicker_length").attr("disabled", "disabled");
        } else {
            $("#datepicker_length").removeAttr("disabled");
            $("#datepicker_length").focus();
        }
    });
});

function pickDates(){
    // reload the page w/ the new dates
    endValue = $("#datepicker_end").val(); 
    try{
        endDate = new Date(endValue);
    } catch {
        endDate = new Date(Date.now())
    }
    endDate.setDate( endDate.getDate() + 1);
    startDate = new Date(endValue);
    startDate.setDate( startDate.getDate() + 1);
    length = $("#datepicker_length").val();
    range = $("#datepicker_range").val();

    if (range == "days") {
        startDate.setDate( startDate.getDate() - length );
    } else if (range == "weeks") {
        weeks = length * 7;
        startDate.setDate( startDate.getDate() - weeks );
    } else if (range == "months" ) {
        startDate.setMonth( startDate.getMonth() - length );
    } else if (range == "years" ){
        startDate.setYear( startDate.getFullYear() - length );
    } else {
        endDate = new Date(Date.now())
        startDate = new Date('1970-01-01T00:00:00')
    }

    start =  startDate.getFullYear() + "-" + (startDate.getMonth() + 1) + "-" + startDate.getDate();
    end =  endDate.getFullYear() + "-" + (endDate.getMonth() + 1) + "-" + endDate.getDate();
    query_string_json.start = start;
    query_string_json.end = end;
    for (query in query_string_json){ 
        query_params.push(query+"="+ query_string_json[query]);
    }
    query_string += query_params.join("&")
    location.href = window.location.href.split('?')[0]+query_string;
};


function setupDatePicker(datepicker_cal, datepicker_hiddenfield){
        $(datepicker_cal).datepicker({
            format: "yyyy-mm-dd",
            todayBtn: "linked",
            startDate: new Date('1970-01-01T00:00:00'),
            endDate: new Date(Date.now())
        });
        $(datepicker_hiddenfield).val(
            $(datepicker_cal).datepicker('getFormattedDate')
        );
        $(datepicker_cal).on('changeDate', function() {
            $(datepicker_hiddenfield).val(
                $(datepicker_cal).datepicker('getFormattedDate')
            );
        });
};

setupDatePicker('#datepicker_start_cal', '#datepicker_start');
setupDatePicker('#datepicker_end_cal', '#datepicker_end');

$( "#datepicker_dates__display" ).click(function() {
    $("#datepicker_modal").slideToggle("slow");
});

$( "#cancelButton" ).click(function() {
    $( "#datepicker_modal" ).slideToggle("fast");
    // reset the calendar values if user cancels out of datepicker_modal using the value of their corresponding date_display value
    $('#datepicker_start_cal').datepicker('update', $('#start_date_display').attr('data-date'));
    $('#datepicker_end_cal').datepicker('update', $('#end_date_display').attr('data-date'));
});