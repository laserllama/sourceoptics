# Copyright 2019 SourceOptics Project Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from django.core.management.base import BaseCommand
from ...models import Author, Repository, Organization, Log
import json
import os
import datetime

USERS = 'users'
REPOS = 'repos'
LOGS = 'logs'
MODES = [ USERS, REPOS, LOGS ]

# short version
# python3 manage.py json_import -m authors -f students.json -c students -o csc492-fall2020
# python3 manage.py json_import -m repos -f teams.json -c teams -o csc492-fall2020

# longer version, allowing site/usage specific JSON choices
# python3 manage.py json_import --file students.json -o csc492-fall2020 --mode=authors --collection-key students --first-name-key firstName --last-name-key lastName --email-key email
# python3 manage.py json_import --file teams.json -o csc492-fall2020 --mode=repos --collection-key teams --repo-url-key repositoryURL --repo-name-key projectTitle
# python3 manage.py json_import --file logs.json -o csc492-fall2020 --mode=logs --collection-key logs --repo-url-key repositoryURL --repo-name-key projectTitle


MODE_MAP = dict(
    users='users_import',
    repos='repos_import',
    logs='logs_import'
)

USER_KEY_CHOICES = [ 'users', 'students' ]
REPO_KEY_CHOICES = [ 'repos', 'projects', 'teams' ]

FIRST_NAME_CHOICES = [ 'first_name', 'firstName', 'firstname', 'first', 'usr_firstname']
LAST_NAME_CHOICES  = [ 'last_name', 'lastName', 'lastname', 'last', 'usr_lastname']
EMAIL_CHOICES      = [ 'email', 'usr_email' ]
REPO_NAME_CHOICES  = [ 'projectTitle', 'project_title', 'project', 'repo_name', 'team_name', 'service_name', 'service' ]
REPO_URL_CHOICES   = [ 'repositoryUrl', 'repositoryURL', 'repository_url', 'repo_url', 'repoUrl', 'whereMyReposAt']
LOG_HOURS_CHOICES  = [ 'log_hours', 'logHours', 'hours' ]
LOG_COMMENTS_CHOICES = [ 'log_comments', 'logComments', 'comments' ]
LOG_DATE_CHOICES = [ 'log_date', 'logDate', 'date']
LOG_TYPE_CHOICES = [ 'log_type', 'logType', 'type', 'ltp_name' ]
LOG_ID_CHOICES = [ 'log_id', 'logId' ]

def guess_first_name(self, ds):
    return self._get_first(ds, FIRST_NAME_CHOICES, self.kwargs['first_name_key'])

def guess_last_name(self, ds):
    return self._get_first(ds, LAST_NAME_CHOICES, self.kwargs['last_name_key'])

def guess_email(self, ds):
    return self._get_first(ds, EMAIL_CHOICES, self.kwargs['email_key'])

def guess_repo_name(self, ds):
    return self._get_first(ds, REPO_CHOICES, self.kwargs['repo_name_key'])

def guess_repo_url(self, ds):
    return self._get_first(ds, REPO_URL_CHOICES, self.kwargs['repo_url_key'])

def guess_log_hours(self, ds):
    return self._get_first(ds, LOG_HOURS_CHOICES, self.kwargs['log_hours_key'])

def guess_log_comments(self, ds):
    return self._get_first(ds, LOG_COMMENTS_CHOICES, self.kwargs['log_comments_key'])

def guess_log_date(self, ds):
    return self._get_first(ds, LOG_DATE_CHOICES, self.kwargs['date_key'])

def guess_log_id(self, ds):
    return self._get_first(ds, LOG_ID_CHOICES, self.kwargs['log_id_key'])

def guess_log_type(self, ds):
    return self._get_first(ds, LOG_TYPE_CHOICES, self.kwargs['log_type_key'], 'Individual')


class Command(BaseCommand):

    __slots__ = ('org', 'data', 'mode', 'is_list', 'ds_type', 'kwargs')

    help = 'imports certain types of objects from specially structured JSON files'

    # ------------------------------------------------------------------------------------------------------------------

    def add_arguments(self, parser):

        # common arguments
        parser.add_argument('-F', '--file', dest='filename', type=str, help='', default=None, required=True)
        parser.add_argument('-o', '--organization', dest='org', type=str, help='add objects to this organization', required=True, default=None)
        parser.add_argument('-m', '--mode', dest='mode', type=str, help='import type', default=None, choices=MODES, required=True)
        parser.add_argument('-c', '--collection-key', dest='collection_key', type=str, help='name of a list item inside the JSON to process, otherwise assume JSON is a list', default=None)

        # tolerance of different/inconsistent json formats - default values supplied
        parser.add_argument('-f', '--first-name-key', dest='first_name_key', default='firstName', type=str, help='')
        parser.add_argument('-l', '--last-name-key', dest='last_name_key', default='lastName', type=str, help='')
        parser.add_argument('-e', '--email-key', dest='email_key', default='email', type=str, help='')
        parser.add_argument('-u', '--repo-url-key', dest='repo_url_key', default='repositoryURL', type=str, help='')
        parser.add_argument('-r', '--repo-name-key', dest='repo_name_key', default='projectTitle', type=str, help='')

        parser.add_argument('-H', '--log-hours-key', dest="log_hours_key", default='log_hours', type=str, help='')
        parser.add_argument('-C', '--log-comments-key', dest="log_comments_key", default='log_comments', type=str, help='')
        parser.add_argument('-t', '--log-type-key', dest="log_type_key", default='log_type', type=str, help='')
        parser.add_argument('-i', '--log-id-key', dest="log_id_key", default='log_id', type=str, help='')
        parser.add_argument('-d', '--date-key', dest="date_key", default='log_date', type=str, help='')


    # ------------------------------------------------------------------------------------------------------------------

    def _load_org(self):
        org = self.kwargs.get('org', None)
        if org:
            return Organization.objects.get(name=org)
        else:
            return None

    # ------------------------------------------------------------------------------------------------------------------

    def _load_data(self):
        filename = self.kwargs['filename']
        filename = os.path.expanduser(filename)
        if not os.path.exists(filename):
            raise Exception("missing file")
        return json.loads(open(filename).read())

    # ------------------------------------------------------------------------------------------------------------------

    def handle(self, *args, **kwargs):

        self.kwargs = kwargs
        self.org = self._load_org()
        self.data = self._load_data()
        self.ds_type = type(self.data)
        self.mode = self.kwargs['mode']

        getattr(self, MODE_MAP[self.mode])()

    # ------------------------------------------------------------------------------------------------------------------

    def _generic_import(self, add_fn):
        if self.ds_type == list:
            for v in self.data:
                add_fn(v)
        elif self.ds_type == dict:
            collection = self._guess_collection()
            for v in collection:
                add_fn(v)
        else:
            raise Exception("expecting a string or a list")

    # ------------------------------------------------------------------------------------------------------------------

    def users_import(self):
        self._generic_import(self.add_user)

    # ------------------------------------------------------------------------------------------------------------------

    def repos_import(self):
        self._generic_import(self.add_repo)

    # ------------------------------------------------------------------------------------------------------------------

    def logs_import(self):
        self._generic_import(self.add_logs)

    # ------------------------------------------------------------------------------------------------------------------

    def add_user(self, ds):

        first_name = self.guess_first_name(ds)
        last_name = self.guess_last_name(ds)
        email = self.guess_email(ds)



        created = True
        (obj, created) = Author.objects.get_or_create(
            display_name  = "%s %s" % (first_name, last_name),
            email = email
        )

        if created:
            print("USER: %s, %s <%s>" % (first_name, last_name, email))


    # ------------------------------------------------------------------------------------------------------------------

    def add_logs(self, ds):

        # this will also create the user if not there, because it needs it

        first_name = self.guess_first_name(ds)
        last_name = self.guess_last_name(ds)
        email = self.guess_email(ds)

        created = True
        (author_obj, created) = Author.objects.get_or_create(
            display_name  = "%s %s" % (first_name, last_name),
            email = email
        )

        if created:
            print("ALSO CREATED USER: %s, %s <%s>" % (first_name, last_name, email))

        hours = guess_log_hours(self, ds)
        date = guess_log_date(self, ds)
        comments = guess_log_comments(self, ds)
        log_type = guess_log_type(self, ds)
        log_id = guess_log_id(self, ds)

        (log_obj, created) = Log.objects.get_or_create(
            log_id = log_id,
            organization = self.org,
            author = author_obj,
            date = date
        )

        date = datetime.datetime.strptime(date, '%Y-%m-%d')

        log_obj.author = author_obj
        log_obj.log_type = log_type
        log_obj.date = date
        log_obj.comments = comments
        log_obj.hours = float(hours)
        log_obj.save()

        print("LOG SAVED: %s" % log_obj)



    # ------------------------------------------------------------------------------------------------------------------

    def _get_first(self, ds, key_choices, input_choice, default=None):

        options = key_choices[:]
        options.insert(0, input_choice)

        for k in key_choices:
            if k in ds:
                return ds[k]

        if default:
            return default

        raise Exception("key not found: %s in %s" % (key_choices, ds))

    # ------------------------------------------------------------------------------------------------------------------

    def _guess_collection(self):
        if self.mode == USERS:
            return self._get_first(self.data, USER_KEY_CHOICES, self.kwargs['collection_key'])
        elif self.mode == REPOS:
            return self._get_first(self.data, REPO_KEY_CHOICES, self.kwargs['collection_key'])
        else:
            return self._get_first(self.data, [], self.kwargs['collection_key'])

    def guess_first_name(self, ds):
        return self._get_first(ds, FIRST_NAME_CHOICES, self.kwargs['first_name_key'])

    def guess_last_name(self, ds):
        return self._get_first(ds, LAST_NAME_CHOICES, self.kwargs['last_name_key'])

    def guess_email(self, ds):
        return self._get_first(ds, EMAIL_CHOICES, self.kwargs['email_key'])

    def guess_repo_name(self, ds):
        return self._get_first(ds, REPO_NAME_CHOICES, self.kwargs['repo_name_key'])

    def guess_repo_url(self, ds):
        return self._get_first(ds, REPO_URL_CHOICES, self.kwargs['repo_url_key'])

    # ------------------------------------------------------------------------------------------------------------------

    def fix_url(self, input_url):

        # this implementation needs to be modified to support other git hosts (PATCHES WELCOME)
        # SSH is required for private repo access.

        # IN: "https://github.ncsu.edu/user_or_org/pathname"
        # OUT => git@github.ncsu.edu:engr-csc-sdc/2020FallTeam16.git

        tokens = input_url.split('/')

        if input_url.startswith('git@'):
            # looks like SSH, great, just use it
            return input_url

        if 'http' in tokens[0] and 'github' in tokens[2]:
            # "/" is tokens[1]

            ssh_user = "git"
            server = tokens[2]
            git_org = tokens[3]
            project = tokens[4]
            return (project, "%s@%s:%s/%s.git" % (ssh_user, server, git_org, project))


        raise Exception("URL conversion for this source not yet supported, file a bug or send us a patch!")

    # ------------------------------------------------------------------------------------------------------------------


    def add_repo(self, ds):

        (project, url) = self.fix_url(self.guess_repo_url(ds))
        name = self.guess_repo_name(ds).replace(" ","_")
        use_name = "%s__%s" % (project, name)

        (obj, created) = Repository.objects.get_or_create(
            name = use_name[:64],
            url = url,
            enabled = True,
            organization = self.org
        )
        if created:
            print("REPO: %s, %s" % (url, use_name))


